<?php

namespace App\Http\Controllers;

use App\Summernote;
use Illuminate\Http\Request;

class BlogController extends Controller {
    
    public function index() {
        $posts = Summernote::all()->paginate('15');
        return view();
    }

    public function create() {
    }

    public function store(Request $request) {
    }

    public function show(Summernote $summernote) {
    }

    public function edit(Summernote $summernote) { 
    }

    public function update(Request $request, Summernote $summernote) {
    }

    public function destroy(Summernote $summernote) {

    }
}
